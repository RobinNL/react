import React from 'react';

import styled from 'styled-components';
import { Button } from '@poc/ui';
import { Navigation } from '@poc/container';

const StyledApp = styled.div`
  font-family: sans-serif;
  min-width: 300px;
  margin: 150px auto;
`;

const Container = styled.div`
    max-width: 1200px;
    margin: 0 auto;
`;

export const App = () => {

  return (
      <StyledApp>
          <Navigation />
          <Container>
             <p>This proof of concept is written in React</p>
          </Container>
      </StyledApp>
  )

};

export default App;
