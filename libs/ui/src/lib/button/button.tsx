import React from "react";

import styled from "styled-components";

/* eslint-disable-next-line */
export interface ButtonProps {}

const StyledButton = styled.button`
  color: pink;
  border: 2px solid red;
`;

export const Button = (props: ButtonProps) => {
  return (
    <StyledButton>
      <button>Welcome to button!</button>
    </StyledButton>
  );
};

export default Button;
