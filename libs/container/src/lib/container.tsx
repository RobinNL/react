import React from "react";

import styled from "styled-components";

/* eslint-disable-next-line */
export interface ContainerProps {}

const StyledContainer = styled.div`
  color: pink;
`;

export const Container = (props: ContainerProps) => {
  return (
    <StyledContainer>
      <h1>Welcome to container!</h1>
    </StyledContainer>
  );
};

export default Container;
