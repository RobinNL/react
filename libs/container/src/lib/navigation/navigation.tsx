import React from "react";

import styled from "styled-components";

/* eslint-disable-next-line */
export interface NavigationProps {}

const StyledNavigation = styled.div`
div {
    display: flex;
    height: 100px;
    top: 0;
    align-items: center;
    justify-content: center;
    position: fixed;
    background-color: #eee;
    width: 100%;
  }

  a {
    color: deepskyblue;
    text-decoration: none;
    font-weight: bold;
  }

  ul {
    max-width: 1200px;
  }

  li {
    float: left;
    list-style-type: none;
    margin-right: 32px;
  }
`;

export const Navigation = (props: NavigationProps) => {
  return (
    <StyledNavigation>
        <div>
            <ul>
                <li>
                    <a href="/basic">Basic components</a>
                </li>
                <li>
                    <a href="/animation">Animation components</a>
                </li>
                <li>
                    <a href="/form">Form components</a>
                </li>
                <li>
                    <a href="/data">Data components</a>
                </li>
            </ul>
        </div>
    </StyledNavigation>
  );
};

export default Navigation;
